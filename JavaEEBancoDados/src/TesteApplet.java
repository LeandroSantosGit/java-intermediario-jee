import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class TesteApplet extends Applet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1113866038036531615L;
	
	private String mensagem; 
	
	@Override
	public void init() { //executar quaniniciar applet
		
		mensagem = getParameter("mensagem");
	}
	
	@Override
	public void paint(Graphics page) { //executado quando tiver que escrever algo no applet
		
		//desenhar um quadrados com 4 cores
		page.setColor(Color.RED); //cor vemelha
		page.fillRect(0, 0, 50, 50); //desenhar o retangulo
		page.setColor(Color.BLUE); //cor azul
		page.fillRect(50, 0, 50, 50);
		page.setColor(Color.GREEN); //cor verde
		page.fillRect(0, 50, 50, 50);
		page.setColor(Color.ORANGE); //laranja
		page.fillRect(50, 50, 50, 50);
		page.setColor(Color.BLACK); //preto
		page.drawString(mensagem, 10, 40);
	}

}
