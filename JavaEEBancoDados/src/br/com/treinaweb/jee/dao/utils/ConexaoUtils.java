package br.com.treinaweb.jee.dao.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexaoUtils {
	
	//conexao com bando de dados
	public static Connection criarConexao() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		
		String stringconexao = "jdbc:mysql://localhost:3307/tw_cadastro?user=root&password=1234";
		Connection conn = DriverManager.getConnection(stringconexao);
		return conn;
	}
	
	//fechar conexao
	public static void fecharConexao (Connection conn) throws SQLException {
		if (conn != null) {
			conn.close();
		}
	}
}
