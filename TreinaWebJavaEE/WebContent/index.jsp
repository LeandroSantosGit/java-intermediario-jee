<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%@include file="Menu.jsp"%>
	<!--Barra de menu -->
	<fieldset style="width: 200px">
		<form method="post" action="infoUsuario.jsp">
			<legend>Informa��es do usuario</legend>
			<div style="font-weight: bold;">Nome completo:</div>
			<div>
				<input type="text" name="nomeCompleto" />
			</div>
			<div style="font-weight: bold;">Nome usuario:</div>
			<div>
				<input type="text" name="nomeUsuario" />
			</div>
			<div style="font-weight: bold;">Senha:</div>
			<div>
				<input type="password" name="senha" />
			</div>
			<div>
				<input type="submit" value="Enviar" />
			</div>
		</form>
	</fieldset>
	<%!//Metodo para pegar a data atual
	public String getDataAtual() {
		return new Date().toString();
	}%>

	<h1>Bem-Vindo ao Curso de Java Intermadiario do TreinaWeb!</h1>
	<%
		String mensagem = "";
		mensagem = "Estou testando o JSP";
		out.println(mensagem);
	%>
	<p>
		A data atual �
		<%=getDataAtual()%></p>

	<!-- Estrutura de repeticao, tabuada de soma 2 -->
	<%
		String tabela = "";
		tabela = "<table>";
		for (int i = 0; i <= 10; i++) {
			int resultado = 2 * i;
			tabela += "<tr>";
			tabela += "<td>";
			tabela += "2 x " + i + " = ";
			tabela += "</td>";
			tabela += "<td>";
			tabela += resultado;
			tabela += "</td>";
			tabela += "</tr>";
		}
		tabela += "</table>";
		out.println(tabela);
	%>

	<!-- Estrutura de decisao, baseado no horario, mes exibir boas vindas ao usuario -->
	<br>
	<%
		//hora
		Calendar data = Calendar.getInstance();
		int hora = data.get(Calendar.HOUR_OF_DAY);

		if (hora >= 6 && hora <= 12) {
			out.println("Bom dia!!!");
		} else if (hora >= 12 && hora <= 18) {
			out.println("Boa tarde!!!");
		} else if (hora >= 18 && hora <= 24) {
			out.println("Boa noite!!!");
		} else {
			out.println("Boa madrugada");
		}

		//mes
		int mes = data.get(Calendar.MONTH) + 1; //calendario + 1 para come�ar no mes de janeiro

		switch (mes) {
		case 1:
			out.println("Janeiro");
			break;
		case 2:
			out.println("Fevereiro");
			break;
		case 3:
			out.println("Mar�o");
			break;
		case 4:
			out.println("Abril");
			break;
		case 5:
			out.println("Maio");
			break;
		case 6:
			out.println("Junho");
			break;
		case 7:
			out.println("Julho");
			break;
		case 8:
			out.println("Agosto");
			break;
		case 9:
			out.println("Setembro");
			break;
		case 10:
			out.println("Outubro");
			break;
		case 11:
			out.println("Novembro");
			break;
		case 12:
			out.println("Dezembro");
			break;
		}
	%>

	<!-- Isso � um comentario em Html -->
</body>
</html>