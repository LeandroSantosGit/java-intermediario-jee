<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"  %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<sql:setDataSource var="conn" dataSource="jdbc:mysql://127.0.0.1:3307/tw_cadastro,com.mysql.jdbc.Driver	,root,1234"/>
	<sql:update dataSource="${conn }" var="cont">
		INSERT INTO usr_usuarios (usr_nome, usr_login, usr_senha) VALUES (?, ?, ?)
		<sql:param value="${param.nomeUsuario }"></sql:param>
		<sql:param value="${param.login }"></sql:param>
		<sql:param value="${param.senha }"></sql:param>
	</sql:update>
	<c:if test="${cont > 0 }">
		<c:redirect url="TagSetDatasource.jsp"></c:redirect>
	</c:if>
	<c:if test="${cont <= 0 }">
		<c:out value="Houve um erro ao cadastrar o usuario"></c:out>
	</c:if>
</body>
</html>